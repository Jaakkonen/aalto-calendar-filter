# Aalto Calendar Sync
This tool allows you to filter Aalto MyCourses vCalendar to have only excercise groups you want.

## Getting started

Go to [MyCourses](https://mycourses.aalto.fi/calendar/export.php) and get subscription calendar link.
Copy `userid` and `authtoken` to the following link
```
  https://aaltocalendar.jaakkonen.now.sh/api/filter
    ?userid=*replace me*
    &authtoken=*replace me*
    &filter=LC-1117,H05
    &filter=MS-A0111,H03
    &filter=MS-A0111,H03
    &filter=*course code*,*group code*
```

The first part of filters tells course name or code and second part the group.
If any of the groups for given course match with a event it will be included in filtered output.
Courses without filters are included in output.

This link can then be imported to Google Calendar or whatever you use :)

Note: For FOSS Android you can use [ICSx5](https://f-droid.org/packages/at.bitfire.icsdroid/) and [Simple Calendar](https://f-droid.org/en/packages/com.simplemobiletools.calendar.pro/). Just Simple Calendar won't do as it doesn't have [network access](https://github.com/SimpleMobileTools/Simple-Calendar/issues/236) for security reasons.

## Usage

### Local
Uncomment local server code from `calendar.py`. API is then exposed at `http://localhost:8080/`

### Prod
Url is: https://aaltocalendar.jaakkonen.now.sh/api/filter

## Deployment
Deploy this project with `now` from Zeit
```
  npm i -g now
  now
```
