from http.server import BaseHTTPRequestHandler
from ics import Calendar
from urllib.request import urlopen
from urllib.parse import urlparse, parse_qs, urlencode
from collections import defaultdict
import re
from typing import Dict

def get_calendar(userid, authtoken):
  url = f"https://mycourses.aalto.fi/calendar/export_execute.php?userid={userid}&authtoken={authtoken}&preset_what=all&preset_time=recentupcoming"
  
  # For some absurd reason Aalto's MyCourses exports multiple CATEGORIES lines per VENVENT
  # And that's not spec compliant
  # Let's fix it by ourselves as a world-class university cannot do that
  ics_t = urlopen(url).read().decode()
  ics_t = re.sub(r"CATEGORIES:(.+?(?=\n\w))\nCATEGORIES:(.+?(?=\n\w))", "CATEGORIES:\\1  ,\\2\\n", ics_t, 0, re.MULTILINE | re.DOTALL)
  return Calendar(ics_t)

def get_filter(filter_parameters):
  # Change filters to dict of {"Course name": "Lecture group"}
  filters = defaultdict(list)
  for filter_param in filter_parameters:
    course_name, group_name = filter_param.split(",")
    filters[course_name].append(group_name) 

  # Return True if removing event.
  def do_filter(event):
    event_name = event.name
    for course_name, group_names in filters.items():
      # If course name is same
      if course_name in event_name:
        # If none of the group names matches, return True and remove
        return all(group_name not in event_name for group_name in group_names)
    # If no filter matches don't remove it
    return False
  
  return do_filter

def parse_calendar(calendar, filter_parameters):
  new_events = calendar.events.copy()
  for event in filter(get_filter(filter_parameters), calendar.events):
    new_events.remove(event)
  calendar.events = new_events
  return calendar

class handler(BaseHTTPRequestHandler):
  def respond(self, body: str, status: int = 200, headers: Dict[str,str] = {}):
      if 'Content-Type' not in headers:
          headers['Content-Type'] = 'text/plain; charset=utf-8'
      self.send_response(status)
      for hn, hv in headers.items():
          self.send_header(hn, hv)
      self.end_headers()
      self.wfile.write(body.encode())
      
    
  def do_GET(self):
    o = urlparse(self.path)
    params = parse_qs(o.query)
    if 'userid' not in params or 'authtoken' not in params:
      self.respond("No userid or authtoken", status=400)
      return
    
    try:
      c = get_calendar(params['userid'][0], params['authtoken'][0])
    except Exception as e:
      print(e)
      self.respond(f"Failed to get calendar. Faulty authcode or userid?\n{e}", status=500)
      return

    try:
      c = parse_calendar(c, params['filter'])
    except Exception as e:
      self.respond(f"Could not filter calendar. Are filters valid?\n{e}")
      return

    self.respond(str(c))


if __name__ == "__main__":
    from http.server import HTTPServer
    server = HTTPServer(('localhost', 8080), handler)
    print('Starting server, use <Ctrl-C> to stop')
    server.serve_forever()
